package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SeparateChainingTest {

	private SeparateChaining <String, String> traductor;
	@Test
	public void testConstructor()
	{
		traductor = new SeparateChaining<>(7);
		assertNotNull(traductor);
	}
	@Test
	public void testPut()
	{
		traductor = new SeparateChaining<>(7);
		traductor.put("pez", "fish");
		assertTrue(traductor.contains("pez"));
		traductor.put("perro", "dog");
		assertTrue(traductor.contains("perro"));
		traductor.put("gato", "cat");
		assertTrue(traductor.contains("gato"));
	}
	@Test
	public void testGet()
	{
		traductor = new SeparateChaining<>(7);
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		String valor = traductor.get("pez");
		assertEquals(valor, "fish");
		valor = traductor.get("gato");
		assertEquals(valor, "cat");
		valor = traductor.get("perro");
		assertEquals(valor , "dog");
	}	

	@Test
	public void testDelete()
	{
		traductor = new SeparateChaining<>(7);
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		traductor.delete("gato");
		assertFalse(traductor.contains("gato"));
		traductor.delete("perro");
		assertFalse(traductor.contains("perro"));
		traductor.delete("pez");
		assertFalse(traductor.contains("pez"));
		
	}
	
}
