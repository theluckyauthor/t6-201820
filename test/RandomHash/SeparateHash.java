package RandomHash;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.data_structures.SeparateChaining;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import model.data_structures.LinearProbing;

public class SeparateHash {

	private SeparateChaining <Integer, Integer> random;
	@Test
	public void test333333()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new SeparateChaining<>(333333);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 333333 y separate chaining");
		// 10000 busquedas
		inicio = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			Integer valor = random.get(azar);
		}
		fin = System.currentTimeMillis();
		System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 333333 y separate chaining");
		assertNotNull(random);
	}
	@Test
	public void test200000()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new SeparateChaining<>(200000);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 200000 y separate chaining");
		// 10000 busquedas
				inicio = System.currentTimeMillis();
				for (int i = 0; i < 10000; i++) 
				{
					int azar = (int) Math.floor(Math.random() * 5000000);
					Integer valor = random.get(azar);
				}
				fin = System.currentTimeMillis();
				System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 200000 y separate chaining");
				assertNotNull(random);
	}
	@Test
	public void test142857()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new SeparateChaining<>(142857);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 142857 y separate chaining");
		// 10000 busquedas
				inicio = System.currentTimeMillis();
				for (int i = 0; i < 10000; i++) 
				{
					int azar = (int) Math.floor(Math.random() * 5000000);
					Integer valor = random.get(azar);
				}
				fin = System.currentTimeMillis();
				System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 142857 y separate chaining");
				assertNotNull(random);
	}
}
