package RandomHash;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.data_structures.LinearProbing;
public class LinearHash {

	private LinearProbing <Integer, Integer> random;
	@Test
	public void test4000000()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new LinearProbing<>(4000000);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 4 millones y linear probing");
		// 10000 busquedas
		inicio = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			Integer valor = random.get(azar);
		}
		fin = System.currentTimeMillis();
		System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 4 millones y linear probing");
		assertNotNull(random);
	}
	@Test
	public void test2000000()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new LinearProbing<>(2000000);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 2 millones y linear probing");
		// 10000 busquedas
				inicio = System.currentTimeMillis();
				for (int i = 0; i < 10000; i++) 
				{
					int azar = (int) Math.floor(Math.random() * 5000000);
					Integer valor = random.get(azar);
				}
				fin = System.currentTimeMillis();
				System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 2 millones y linear probing");
				assertNotNull(random);
	}
	@Test
	public void test1333333()
	{
		// construir
		long inicio = System.currentTimeMillis();
		random = new LinearProbing<>(1333333);
		for (int i = 0; i < 1000000; i++) 
		{
			int azar = (int) Math.floor(Math.random() * 5000000);
			int azar2 = (int) Math.floor(Math.random() * 5000000);
			random.put(azar, azar2);
		}
		long fin = System.currentTimeMillis();
		System.out.println( " Con 1 millon de datos se tarda " + (fin-inicio) + " milisegundos en terminar de crear la tabla usando m = 1.33 millones y linear probing");
		// 10000 busquedas
				inicio = System.currentTimeMillis();
				for (int i = 0; i < 10000; i++) 
				{
					int azar = (int) Math.floor(Math.random() * 5000000);
					Integer valor = random.get(azar);
				}
				fin = System.currentTimeMillis();
				System.out.println( " El tiempo para 10000 busquedas es " + ((fin-inicio)) + " milisegundos para la tabla usando m = 1.33 millones y linear probing");
				assertNotNull(random);
	}
	
}
