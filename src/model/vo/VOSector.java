package model.vo;

import model.data_structures.DoublyLinkedList;

public class VOSector {

	private double longitudMinima;
	private double longitudMaxima;
	private double latitudMinima;
	private double latitudMaxima;
	private int id;
	private DoublyLinkedList<VOBikeRoute> rutas;
	
	public DoublyLinkedList<VOBikeRoute> getRutas() {
		return rutas;
	}

	public void setRutas(DoublyLinkedList<VOBikeRoute> rutas) {
		this.rutas = rutas;
	}

	public VOSector(int longitud, int latitud, double pLongMinima, double  pLongMaxima, double  pLatMinima, double pLatMaxima)
	{
		setId((10* longitud)  + (latitud +1 ));
		setLongitudMinima(pLongMinima);
		setLongitudMaxima(pLongMaxima);
		setLatitudMinima(pLatMinima);
		setLatitudMaxima(pLatMaxima);
		rutas = new DoublyLinkedList<>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLongitudMinima() {
		return longitudMinima;
	}

	public void setLongitudMinima(double longitudMinima) {
		this.longitudMinima = longitudMinima;
	}

	public double getLongitudMaxima() {
		return longitudMaxima;
	}

	public void setLongitudMaxima(double longitudMaxima) {
		this.longitudMaxima = longitudMaxima;
	}

	public double getLatitudMinima() {
		return latitudMinima;
	}

	public void setLatitudMinima(double latitudMinima) {
		this.latitudMinima = latitudMinima;
	}

	public double getLatitudMaxima() {
		return latitudMaxima;
	}

	public void setLatitudMaxima(double latitudMaxima) {
		this.latitudMaxima = latitudMaxima;
	}
	public void agregarRuta(VOBikeRoute v)
	{
		rutas.addAtBeginning(v);
	}
}
