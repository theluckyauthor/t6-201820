package model.vo;

public class VOBikeRoute {

	private String id;
	private String type;
	private String[] the_geom; // arreglo con elementos de la forma <long lat>
	private String street;
	private String fStreet;
	private String tStreet;
	private double shapeLeng;
	
	public VOBikeRoute( String pId, String pType, String[] pThe_geom, String pStreet, String pFStreet, String pTStreet, double pShapeLeng)
	{
		setId(pId);
		setType(pType);
		setStreet(pStreet);
		setfStreet(pFStreet);
		settStreet(pTStreet);
		setShapeLeng(pShapeLeng);
		setThe_geom(pThe_geom);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getThe_geom() {
		return the_geom;
	}

	public void setThe_geom(String[] the_geom) {
		this.the_geom = the_geom;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getfStreet() {
		return fStreet;
	}

	public void setfStreet(String fStreet) {
		this.fStreet = fStreet;
	}

	public String gettStreet() {
		return tStreet;
	}

	public void settStreet(String tStreet) {
		this.tStreet = tStreet;
	}

	public double getShapeLeng() {
		return shapeLeng;
	}

	public void setShapeLeng(double shapeLeng) {
		this.shapeLeng = shapeLeng;
	}
	
}
