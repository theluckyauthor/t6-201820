package model.logic;

import java.io.File;
import java.io.FileReader;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import api.IDivvyTripsManager;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Iter;
import model.data_structures.LinearProbing;
import model.data_structures.SeparateChaining;
import model.vo.VOBikeRoute;
import model.vo.VOSector;



public class DivvyTripsManager implements IDivvyTripsManager {

	private final static int LONGITUDES = 10;
	private final static int LATITUDES = 10;
	private DoublyLinkedList <VOBikeRoute> bikeRoutes;
	private double longitudMaxima = -1000000;
	private double latitudMaxima = -1000000;
	private double longitudMinima = 1000000;
	private double latitudMinima = 1000000;
	private VOSector[][] sectores;
	private LinearProbing<Integer ,VOSector> sectoreslinear;
	private SeparateChaining<Integer ,VOSector> sectoresseparate;
	
	@Override
	public void loadBikeRoutes(String tripsFile) throws Exception{
		try
		{
			JSONParser parser = new JSONParser();
			bikeRoutes = new DoublyLinkedList<VOBikeRoute>();
			JSONArray arreglo = (JSONArray) parser.parse(new FileReader(tripsFile));
			for (int i = 0; i < arreglo.size(); i++)
			{
				JSONObject s = (JSONObject) arreglo.get(i);
				String id = String.valueOf(s.get("id"));
				String type = String.valueOf(s.get("BIKEROUTE"));
				String the_geom = String.valueOf(s.get("the_geom"));
				String requiredString = the_geom.substring(the_geom.indexOf("(") + 1, the_geom.indexOf(")"));
				String[] longlat = requiredString.trim().split(",");
				for(int j = 0; j < longlat.length; j++)
				{
					String[]longitudlatitud = longlat[j].trim().split(" ");
					double longitud = Double.valueOf(longitudlatitud[0].trim());
					double latitud = Double.valueOf(longitudlatitud[1].trim());
					if(longitud > longitudMaxima)
					{
						longitudMaxima = longitud;
					}
					if(longitud < longitudMinima)
					{
						longitudMinima = longitud;
					}
					if(latitud > latitudMaxima)
					{
						latitudMaxima = latitud;
					}
					if(latitud < latitudMinima)
					{
						latitudMinima = latitud;
					}
				}
				String street = String.valueOf( s.get("STREET"));
				String fStreet = String.valueOf( s.get("FSTREET"));
				String tStreet = String.valueOf(s.get("TSTREET"));
				double shapeLeng = Double.parseDouble(String.valueOf(s.get("Shape_Leng")).trim());
				VOBikeRoute sample = new VOBikeRoute(id, type, longlat, street, fStreet, tStreet, shapeLeng);
				bikeRoutes.addAtBeginning(sample);
				System.out.println("se creo una cicloruta " + i + " con " + longlat.length +  " puntos");
			}
			System.out.println("se crearon todas las ciclorutas");
			prepararSectores();
			System.out.println("se prepararon todos los sectores");
			crearSeparateChaining();
			System.out.println("hash separate chaining");
			crearLinearProbing();
			System.out.println("hash linear probing");
		}
		catch (Exception e)
		{
			throw new Exception("Error cargando el archivo " + tripsFile + "   " + e.getMessage() + "   " + e.getStackTrace());
		}
	}
	
	private void prepararSectores() {
		double steplong = Math.abs(longitudMaxima - longitudMinima)/LONGITUDES;
		double steplat = Math.abs(latitudMaxima - latitudMinima)/LATITUDES;
		double[] puntoslong = new double[LONGITUDES +1];
		double[] puntoslat = new double[LATITUDES +1];
		puntoslong[0] = longitudMinima;
		sectores = new VOSector [LONGITUDES][LATITUDES];
		
		for(int i = 1; i < puntoslong.length; i++)
		{
			puntoslong[i] = puntoslong[i-1] + steplong;
		}
		puntoslat[0] = latitudMaxima;
		for(int i = 1; i < puntoslat.length; i++)
		{
			puntoslat[i] = puntoslat[i-1] - steplat;
		}
		for(int j = 1; j < LONGITUDES +1 ; j++ )
		{
			for(int k = 1; k < LATITUDES + 1; k++)
			{
				int longitud = j - 1;
				int latitud = k - 1;
				double longitudMinima = puntoslong[j -1];
				double longitudMaxima = puntoslong[j];
				double latitudMinima = puntoslat[k];
				double latitudMaxima = puntoslat[k-1];
				VOSector s = new VOSector(longitud, latitud, longitudMinima, longitudMaxima, latitudMinima, latitudMaxima);
				sectores[j-1][k-1] = s;
			}
		}
	}

	private void crearLinearProbing() {
		
		
		VOBikeRoute br = bikeRoutes.getElement(0);
		String[] longlatInicial = br.getThe_geom()[0].trim().split(" ");
		
		String[] longlatFinal = br.getThe_geom()[br.getThe_geom().length -1].trim().split(" ");
		int sectorInicial = darSector(Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1]));
		int sectorFinal = darSector(Double.parseDouble(longlatFinal[0]), Double.parseDouble(longlatFinal[1]));
		int[] sector = darCoordenadasSector(sectorInicial);
		
		if (sectores[sector[0]][sector[1]].getRutas().getSize()==0 ) { //verificar si la lista de ciclorutas esta vacia o si ya se cargó
			if (sectorInicial == sectorFinal) {
				sectores[sector[0]][sector[1]].agregarRuta(br);
			} else {
				int[] sectori = darCoordenadasSector(sectorInicial);
				int[] sectorf = darCoordenadasSector(sectorFinal);
				sectores[sectori[0]][sectori[1]].agregarRuta(br);
				sectores[sectorf[0]][sectorf[1]].agregarRuta(br);
			}
			Iterator<VOBikeRoute> iter = bikeRoutes.iterator();
			while (iter.hasNext()) {
				br = iter.next();
				longlatInicial = br.getThe_geom()[0].trim().split(" ");

				longlatFinal = br.getThe_geom()[br.getThe_geom().length - 1].trim().split(" ");
				sectorInicial = darSector(Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1]));
				sectorFinal = darSector(Double.parseDouble(longlatFinal[0]), Double.parseDouble(longlatFinal[1]));
				if (sectorInicial == sectorFinal) {
					sector = darCoordenadasSector(sectorInicial);
					sectores[sector[0]][sector[1]].agregarRuta(br);
				} else {
					int[] sectori = darCoordenadasSector(sectorInicial);
					int[] sectorf = darCoordenadasSector(sectorFinal);
					sectores[sectori[0]][sectori[1]].agregarRuta(br);
					sectores[sectorf[0]][sectorf[1]].agregarRuta(br);
				}

			} 
		}
		if (sectoreslinear == null) {
			sectoreslinear = new LinearProbing<Integer, VOSector>(LONGITUDES * LATITUDES);
			for (int i = 0; i < sectores.length; i++) {
				for (int j = 0; j < sectores[i].length; j++) {
					VOSector s = sectores[i][j];
					Integer id = s.getId();
					sectoreslinear.put(id, s);
				}
			} 
		}
		
	}

	private void crearSeparateChaining() {
		VOBikeRoute br = bikeRoutes.getElement(0);
		String[] longlatInicial = br.getThe_geom()[0].trim().split(" ");
		String[] longlatFinal = br.getThe_geom()[br.getThe_geom().length -1].trim().split(" ");
		int sectorInicial = darSector(Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1]));
		int sectorFinal = darSector(Double.parseDouble(longlatFinal[0]), Double.parseDouble(longlatFinal[1]));
		int[] sector = darCoordenadasSector(sectorInicial);
		
		if (sectores[sector[0]][sector[1]].getRutas().getSize()==0 ) { //verificar si la lista de ciclorutas esta vacia o si ya se cargó
			if (sectorInicial == sectorFinal) {
				sectores[sector[0]][sector[1]].agregarRuta(br);
			} else {
				int[] sectori = darCoordenadasSector(sectorInicial);
				int[] sectorf = darCoordenadasSector(sectorFinal);
				sectores[sectori[0]][sectori[1]].agregarRuta(br);
				sectores[sectorf[0]][sectorf[1]].agregarRuta(br);
			}
			Iterator<VOBikeRoute> iter = bikeRoutes.iterator();
			while (iter.hasNext()) {
				br = iter.next();
				longlatInicial = br.getThe_geom()[0].trim().split(" ");

				longlatFinal = br.getThe_geom()[br.getThe_geom().length - 1].trim().split(" ");
				sectorInicial = darSector(Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1]));
				sectorFinal = darSector(Double.parseDouble(longlatFinal[0]), Double.parseDouble(longlatFinal[1]));
				if (sectorInicial == sectorFinal) {
					sector = darCoordenadasSector(sectorInicial);
					sectores[sector[0]][sector[1]].agregarRuta(br);
				} else {
					int[] sectori = darCoordenadasSector(sectorInicial);
					int[] sectorf = darCoordenadasSector(sectorFinal);
					sectores[sectori[0]][sectori[1]].agregarRuta(br);
					sectores[sectorf[0]][sectorf[1]].agregarRuta(br);
				}

			} 
		}
		if (sectoresseparate == null) {
			sectoresseparate = new SeparateChaining<>(LONGITUDES * LATITUDES);
			for (int i = 0; i < sectores.length; i++) {
				for (int j = 0; j < sectores[i].length; j++) {
					VOSector s = sectores[i][j];
					Integer id = s.getId();
					sectoresseparate.put(id, s);
				}
			} 
		}
		
	}

	@Override
	public String returnMaxMinLongLat() {
		return "La longitud maxima es:" + longitudMaxima + '\n'
				+ "La latitud maxima es: " + latitudMaxima + '\n'
				+ "La longitud minima es: " + longitudMinima + '\n'
				+ "La latitud minima es: " + latitudMinima;
	}

	public int[] darCoordenadasSector(int sector)
	{
		int[] valores = new int[2];
		if(sector <= 10)
		{
			valores[0] = 0;
			valores[1] = sector - 1;
		}
		else
		{
			sector--;
			String[] arreglo = String.valueOf(sector).split("");
			valores[0] = Integer.valueOf(arreglo[0]);
			valores[1] = Integer.valueOf(arreglo[1]);
		}
		return valores;
	}
	public int darSector(double longitud, double latitud)
	{
		boolean longitudEncontrada = false;
		boolean latitudEncontrada = false;
		int longitudfinal = -1;
		int latitudfinal = -1;
		
		for(int i = 0; i < sectores.length && !longitudEncontrada; i++)
		{
			VOSector s = sectores[i][0];
			if(longitud >= s.getLongitudMinima() && longitud <= s.getLongitudMaxima())
			{
				longitudfinal = i;
				longitudEncontrada = true;
			}
		}
		for(int i = 0; i < sectores[0].length && !latitudEncontrada; i++)
		{
			VOSector s = sectores[0][i];
			if(latitud >= s.getLatitudMinima() && latitud <= s.getLatitudMaxima())
			{
				latitudfinal = i;
				latitudEncontrada = true;
			}
		}
		if(longitudfinal != -1 && latitudfinal!= -1)
		{
			return ((10* longitudfinal)  + (latitudfinal +1 ));
		}
		else
		{
			return -1;
		}
	}
	public boolean estaEnSector(int id, double longitud, double latitud)
	{
		return id == darSector(longitud, latitud);
	}
	@Override
	public void searchLinearProbing(double longitud, double lat) {

		int hipotesis = darSector(longitud, lat);
		if(hipotesis == -1)
		{
			System.out.println("Las coordenadas indicadas están fuera del rango");
		}
		else
		{
			VOSector sector = sectoreslinear.get(hipotesis);
			System.out.println("El punto indicado está en el sector " + hipotesis);
			if(sector.getRutas().getSize() == 0)
			{
				System.out.println("No hay rutas en el sector ");
			}
			else
			{
				VOBikeRoute br = sector.getRutas().getElement(0);
				String[] longlatInicial = br.getThe_geom()[0].trim().split(" ");
				if(estaEnSector(hipotesis, Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1])))
				{
					System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[0].trim());
				}
				else
				{
					System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[br.getThe_geom().length -1].trim());

				}
				Iterator<VOBikeRoute> iter = sector.getRutas().iterator();
				while(iter.hasNext())
				{
					br = iter.next();
					longlatInicial = br.getThe_geom()[0].trim().split(" ");
					if(estaEnSector(hipotesis, Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1])))
					{
						System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[0].trim());
					}
					else
					{
						System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[br.getThe_geom().length -1].trim());

					}
				}
			}
		}
	}

	@Override
	public void searchSeparateChaining(double longitud, double lat) {
		int hipotesis = darSector(longitud, lat);
		if(hipotesis == -1)
		{
			System.out.println("Las coordenadas indicadas están fuera del rango");
		}
		else
		{
			VOSector sector = sectoresseparate.get(hipotesis);
			System.out.println("El punto indicado está en el sector " + hipotesis);
			if(sector.getRutas().getSize() == 0)
			{
				System.out.println("No hay rutas en el sector ");
			}
			else
			{
				VOBikeRoute br = sector.getRutas().getElement(0);
				String[] longlatInicial = br.getThe_geom()[0].trim().split(" ");
				if(estaEnSector(hipotesis, Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1])))
				{
					System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[0].trim());
				}
				else
				{
					System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[br.getThe_geom().length -1].trim());

				}
				Iterator<VOBikeRoute> iter = sector.getRutas().iterator();
				while(iter.hasNext())
				{
					br = iter.next();
					longlatInicial = br.getThe_geom()[0].trim().split(" ");
					if(estaEnSector(hipotesis, Double.parseDouble(longlatInicial[0]), Double.parseDouble(longlatInicial[1])))
					{
						System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[0].trim());
					}
					else
					{
						System.out.println("La cicloruta que pasa por la calle " + br.getStreet() + " comienza en este sector .Su localización es " + br.getThe_geom()[br.getThe_geom().length -1].trim());

					}
				}
			}
		}
	}
}
