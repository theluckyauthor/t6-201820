package model.data_structures;

import java.util.Iterator;

public interface GenericHashTable <K , V >{

	void put(K k, V v);

	V get(K k);

	V delete(K k);
	
	Iterator<K> keys();

}
