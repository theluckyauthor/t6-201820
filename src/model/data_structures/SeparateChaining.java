package model.data_structures;

import java.awt.RenderingHints.Key;
import java.util.Iterator;

public class SeparateChaining <K extends Comparable <K>,V> implements GenericHashTable<K,V>{

	private int n;                                // number of key-value pairs
    private int m;                                // hash table size
    private SequentialSearchST<K, V>[] st;  // array of linked-list symbol tables
	
    public SeparateChaining(int m)
    {
		 this.m = m;
	        st = (SequentialSearchST<K, V>[]) new SequentialSearchST[m];
	        for (int i = 0; i < m; i++)
	            st[i] = new SequentialSearchST<K, V>();
    }
    
    @Override
	public void put(K k, V v) {
		
    	if (k == null) throw new IllegalArgumentException("first argument to put() is null");
        if (v == null) {
            delete(k);
            return;
        }

        // double table size if average length of list >= 10
        if (n >= 5*m) resize(2*m);

        int i = hash(k);
        if (!st[i].contains(k)) n++;
        st[i].put(k, v);

	}

 // resize the hash table to have the given number of chains,
    // rehashing all of the keys
    private void resize(int chains) {
        SeparateChaining<K, V> temp = new SeparateChaining<K, V>(chains);
        for (int i = 0; i < m; i++) {
            for (K key : st[i].keys()) {
                temp.put(key, st[i].get(key));
            }
        }
        this.m  = temp.m;
        this.n  = temp.n;
        this.st = temp.st;
    }

    // hash value between 0 and m-1
    private int hash(K key) {
        return (key.hashCode() & 0x7fffffff) % m;
    } 
    
    public boolean contains(K key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    } 
    
	@Override
	public V get(K k) {
        if (k == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(k);
        return st[i].get(k);
	}

	@Override
	public V delete(K k) {
		if (k == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(k);
        if (st[i].contains(k)) n--;
        V toReturn = st[i].delete(k);
        return toReturn;
	}

	@Override
	public Iterator<K> keys() {
		Queue<K> queue = new Queue<K>();
        for (int i = 0; i < m; i++) {
            for (K key : st[i].keys())
                queue.enqueue(key);
        }
        return queue.iterator();
	}

}
