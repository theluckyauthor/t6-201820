package api;

public interface IDivvyTripsManager {

	void loadBikeRoutes(String tripsFile) throws Exception;
	
	String returnMaxMinLongLat();
	
	void searchLinearProbing(double longitud , double lat);
	
	void searchSeparateChaining(double longitud , double lat);
}
