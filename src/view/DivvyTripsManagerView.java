package view;
import java.util.Scanner;

import controller.Controller;
public class DivvyTripsManagerView {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				try {
					Controller.loadTrips();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			case 2:
				System.out.println(Controller.returnMaxMinLongLat());
				break;

			case 3:

				System.out.println("Digite una longitud");
				double longitud = sc.nextDouble();
				System.out.println("Digite una latitud");
				double latitud = sc.nextDouble();
				Controller.searchLinearProbing(longitud , latitud);
				break;

			case 4:

				System.out.println("Digite una longitud");
				double longi = sc.nextDouble();
				System.out.println("Digite una latitud");
				double lati = sc.nextDouble();
				Controller.searchSeparateChaining(longi , lati);
				break;

			case 5:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("Req. 1: Cargar los datos de viajes registrados en el archivo Divvy_Trips_2017_Q2");
		System.out.println("Req. 2: Dar latitud y longitud maximas y minimas");
		System.out.println("Req. 3: Ordenar la muestra generada por un algoritmo de ordenamiento (Merge). Cronometrar el tiempo que toma realizar el ordenamiento.");
		System.out.println("Req. 4: Ordenar la muestra generada por otro algoritmo de ordenamiento (Quick). Cronometrar el tiempo que toma realizar el ordenamiento.");
		System.out.println("Req. 5: Terminar la ejecucio de la aplicacion");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
