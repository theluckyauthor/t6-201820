package controller;
import api.IDivvyTripsManager;
import model.logic.DivvyTripsManager;
public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager manager = new DivvyTripsManager();

	public static void loadTrips() throws Exception 
	{

		manager.loadBikeRoutes("./data/bikeRoutes.JSON");

	}
	public static String returnMaxMinLongLat()
	{
		return manager.returnMaxMinLongLat();
	}
	public static void searchLinearProbing(double longitud , double lat)
	{
		manager.searchLinearProbing(longitud, lat);
	}
	public static void searchSeparateChaining(double longitud , double lat)
	{
		manager.searchSeparateChaining(longitud, lat);
	}
}
